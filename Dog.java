
public class Dog {
    private String name;
    private  int energy;
    private int hunger;
    private int boredom;
    

    public Dog (String name) {
        this.name=name;
        this.energy=0;
        this.hunger=0;
        this.boredom=0;
    }

    public boolean takeNap() {
        if (this.hunger>50 || this.boredom>50) {
            System.out.println("The dog is too bored or hungry to take a nap.");
            return false;
        }
        else {
            this.energy=this.energy+20;
            this.hunger=this.hunger+5;
            this.boredom=this.boredom+10;
            System.out.println("The dog napped successfully.");
            return true;
        }
    }
    public void eat() {
        this.hunger=this.hunger - 30;
        if (this.hunger <= 0) {
            this.hunger=0;
        }
        System.out.println("The Dog ate.");

    }
    public void play() {
        this.boredom=this.boredom-30;
        this.hunger=this.hunger+5;
        if (this.boredom <= 0) {
            this.boredom=0;
        }
        System.out.println("The dog played with the human");
    }
    public boolean playWith(Human human) {
        if (this.energy < 50) {
            System.out.println("The dog doesn't have energy to play");
            return false;
        }
        else {
            this.energy=this.energy-50;
            human.play();
            return true;
        }
    }
}