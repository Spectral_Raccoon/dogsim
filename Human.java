public class Human {
    private String name;
    private double money;
    private int hunger;
    private int boredom;

    public Human(String name){
        this.name = name;
        this.hunger = 0;
        this.boredom = 0;
        this.money = 0;
    }

    public boolean doWork() {
        if (this.hunger < 50 && this.boredom < 50) {
            this.money = this.money+20;
            this.hunger = this.hunger+5;
            this.boredom = this.boredom+10;
            System.out.println("The human worked successfully!");
            return true;
        } else {
            System.out.println("The human was either too bored or too tired to work.");
            return false;
        }
    }

    public void eat() {
        this.hunger = this.hunger - 30;
        if (this.hunger < 0 ) {
            this.hunger = 0;
        }
        System.out.println("The Human ate.");
    }

    public boolean feed(Dog puppy) {
        if (this.money > 50) {
            this.money = this.money - 50;
            System.out.println("The human bought food.");
            puppy.eat();
            return true;
        } else {
            System.out.println("The human does not have enough money to eat.");
            return false;
        }
    }

    public void play() {
        this.boredom = this.boredom - 30;
        if (this.boredom < 0) {
            this.boredom = 0;
        }
        this.hunger = this.hunger + 5;
        System.out.println("The dog played with the human");
    }

    public int getBoredom() {
        return this.boredom;
    }
}