public class DogOwnerSim {
    public static void main(String[] args) {
        Dog dog = new Dog("Hachi");
        Human human = new Human("Thomas Anderson");

        for (int i = 0; i<human.getBoredom(); i++) {
            human.doWork();
        }
        System.out.println("Human too tired to work.");
    }
}
